var $ = require('jquery');
var ionicBackdrop = require('./backdrop');

var POPUP_TPL =
  '<div class="popup-container" ng-class="cssClass">' +
  '<div class="popup">' +
  '<div class="popup-head">' +
  '<h3 class="popup-title" rv-html="title"></h3>' +
  '<h5 class="popup-sub-title" rv-html="subTitle" rv-if="subTitle"></h5>' +
  '</div>' +
  '<div class="popup-body">' +
  '</div>' +
  '<div class="popup-buttons" rv-show="buttons.length">' +
  '<button rv-each-button="buttons" rv-on-click="buttonTapped" class="button" rv-class="button.type | default \'button-default\'" rv-html="button.text"></button>' +
  '</div>' +
  '</div>' +
  '</div>';

var popup = {

  show: showPopup,

  alert: showAlert,

  confirm: showConfirm,

  //prompt: showPrompt,

  //_createPopup: createPopup,
  //_popupStack: popupStack
};

function createPopup(options) {
  options = $.extend({
    scope: null,
    title: '',
    buttons: []
  }, options || {});

  var self = {};
  self.scope = options.scope || {};
  self.element = $(POPUP_TPL);
  self.responseDeferred = $.Deferred();
  self.responsePromise = self.responseDeferred.promise();

  //$ionicBody.get().appendChild(self.element[0]);
  document.body.appendChild(self.element[0]);
  //$compile(self.element)(self.scope);
  
  var noop = function () { };

  $.extend(self.scope, {
    title: options.title,
    buttons: options.buttons,
    subTitle: options.subTitle,
    cssClass: options.cssClass,
    buttonTapped: function (event, scope) {
      var result = (scope.button.onTap || noop).apply(self, [event]);
      event = event.originalEvent || event; //jquery events

      if (!event.defaultPrevented) {
        self.responseDeferred.resolve(result);
      }
    }
  });

  $.when(
    (options.template || options.content || '')
    ).then(function (template) {
      var popupBody = $(self.element[0].querySelector('.popup-body'));
      if (template) {
        popupBody.html(template);      
        //$compile(popupBody.contents())(self.scope);
      } else {
        popupBody.remove();
      }
      self.view = rivets.bind(self.element, self.scope)
    });

  self.show = function () {
    if (self.isShown || self.removed) return;

    //$ionicModal.stack.add(self);
    self.isShown = true;
    requestAnimationFrame(function () {
      //if hidden while waiting for raf, don't show
      if (!self.isShown) return;

      self.element.removeClass('popup-hidden');
      self.element.addClass('popup-showing active');
      //focusInput(self.element);
    });
  };

  self.hide = function (callback) {
    callback = callback || noop;
    if (!self.isShown) return callback();

    //$ionicModal.stack.remove(self);
    self.isShown = false;
    self.element.removeClass('active');
    self.element.addClass('popup-hidden');
    setTimeout(callback, 250);
  };

  self.remove = function () {
    if (self.removed) return;

    self.hide(function () {
      self.element.remove();
      //self.scope.$destroy();
    });

    self.removed = true;
  };

  return self;
}

var popupStack = [];
var $ionicBody = $(document.body);

function showPopup(options) {
  var popup = createPopup(options);
  var showDelay = 0;

  if (popupStack.length > 0) {
    //showDelay = config.stackPushDelay;
    //$timeout(popupStack[popupStack.length - 1].hide, showDelay, false);
  } else {
    //Add popup-open & backdrop if this is first popup
    $(document.body).addClass('popup-open');
    ionicBackdrop.retain();
    //only show the backdrop on the first popup
    //$ionicPopup._backButtonActionDone = $ionicPlatform.registerBackButtonAction(
    //  onHardwareBackButton,
    //  IONIC_BACK_PRIORITY.popup
    //);
  }

  // Expose a 'close' method on the returned promise
  popup.responsePromise.close = function popupClose(result) {
    if (!popup.removed) popup.responseDeferred.resolve(result);
  };

  doShow();

  return popup.responsePromise;

  function doShow() {
    popupStack.push(popup);
    setTimeout(popup.show, showDelay);

    popup.responsePromise.then(function (result) {
      var index = popupStack.indexOf(popup);
      if (index !== -1) {
        popupStack.splice(index, 1);
      }

      popup.remove();

      if (popupStack.length > 0) {
        popupStack[popupStack.length - 1].show();
      } else {
        ionicBackdrop.release();
        //Remove popup-open & backdrop if this is last popup
        setTimeout(function () {
          // wait to remove this due to a 300ms delay native
          // click which would trigging whatever was underneath this
          if (!popupStack.length) {
            $ionicBody.removeClass('popup-open');
          }
        }, 400);
        //($ionicPopup._backButtonActionDone || noop)();
      }

      return result;
    });
  }
}

function showAlert(opts) {
  return showPopup($.extend({
    buttons: [{
      text: opts.okText || 'OK',
      type: opts.okType || 'button-positive',
      onTap: function () {
        return true;
      }
    }]
  }, opts || {}));
}

function showConfirm(opts) {
  return showPopup($.extend({
    buttons: [{
      text: opts.cancelText || 'Cancel',
      type: opts.cancelType || 'button-default',
      onTap: function () { return false; }
    }, {
        text: opts.okText || 'OK',
        type: opts.okType || 'button-positive',
        onTap: function () { return true; }
      }]
  }, opts || {}));
}

module.exports = popup;