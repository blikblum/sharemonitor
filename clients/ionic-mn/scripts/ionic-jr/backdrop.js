var $ = require('jquery');
var $element = $('<div class="backdrop">');

document.body.appendChild($element[0]);

var backdrop = {
  retain: function() {
    $element.addClass('visible active');      
  },
  release: function() {
    $element.removeClass('visible active');
  }
};

module.exports = backdrop;