import Radio from 'backbone.radio'
import HeaderView from './views/header';

export default Marionette.Application.extend({
  region: '#content',
  initialize: function () {
    this.headerView = new HeaderView({});
    this.headerView.render();
    this.listenTo(Radio.channel('router'), 'route:render', this.onRouteRender);
  },
  onRouteRender: function (route) {
    var contentEl = document.getElementById('content');
    if (this.scrollView) {
      if (this.scrollView.__indicatorX) {
        contentEl.removeChild(this.scrollView.__indicatorX.el)
      }
      if (this.scrollView.__indicatorY) {
        contentEl.removeChild(this.scrollView.__indicatorY.el)
      }
      this.scrollView.__cleanup();
    }
    route.view.$el.addClass('scroll');
    this.headerView.setPageView(route.view);

    this.scrollView = new ionic.views.Scroll({el: contentEl});
    setTimeout(function () {
      this.scrollView && this.scrollView.run();
    }, 0);
  }
});