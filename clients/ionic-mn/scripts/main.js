import './utils/librariesconfig';
import Application from './application';
import ApplicationRoute from './routes/application';
import {middleware, createRouter} from './marionette.routing';
import OwnedSymbolsRoute from './routes/ownedsymbols'
import NewBuyRoute from './routes/newbuy'
import SymbolRoute from './routes/symbol'
import SymbolDetailsRoute from './routes/symboldetails'
import SymbolShareBuyRoute from './routes/symbolsharebuy'

var router = createRouter({log: true, logError: true})

router.map(function (route) {
  route('application', {path: '/', routeClass: ApplicationRoute, abstract: true}, function () {
    route('newbuy', {path: 'newbuy', routeClass: NewBuyRoute})
    route('symbol', {path: 'symbols/:symbolid', routeClass: SymbolRoute,  abstract: true}, function () {
      route('symboldetails', {path: '', routeClass: SymbolDetailsRoute})
      route('symbolnewbuy', {path: 'newbuy', routeClass: NewBuyRoute})
      route('symbolsharebuy',{path: 'sharebuys/:sharebuyid', routeClass: SymbolShareBuyRoute})
    })
    route('ownedsymbols', {path:':path*', routeClass: OwnedSymbolsRoute})
  })
});

router.use(middleware)

var app = new Application();

router.rootRegion = app.getRegion()

app.start();

router.listen()