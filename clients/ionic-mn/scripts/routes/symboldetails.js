import {Route} from '../marionette.routing'
import SymbolDetailsView from '../views/symboldetails'

export default Route.extend({

  viewClass: SymbolDetailsView,

  viewOptions() {
    return {
      model: this.symbol
    }
  },

  activate(transition){
    this.symbol = this.getContext(transition).request('symbol');
  }
})
