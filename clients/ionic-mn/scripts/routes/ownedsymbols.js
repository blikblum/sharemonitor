import Radio from 'backbone.radio'
import {Route} from '../marionette.routing'
import OwnedSymbolsView from '../views/ownedsymbols'


export default Route.extend({

  viewClass: OwnedSymbolsView,

  viewOptions() {
    return {
      collection: this.ownedsymbols
    }
  },

  activate(){
    this.ownedsymbols = Radio.channel('data').request('ownedsymbols')
  }
})
