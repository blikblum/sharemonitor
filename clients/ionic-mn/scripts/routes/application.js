import Radio from 'backbone.radio'
import {Route} from '../marionette.routing'
import dataService from '../services/data'

export default Route.extend({

  initialize() {
    Radio.channel('data').reply('ownedsymbols', this.getOwnedSymbols, this)
  },

  getOwnedSymbols() {
    return this.ownedsymbols
  },

  activate(){
    return dataService.getOwnedSymbols().then((symbols) => {
      this.ownedsymbols = symbols
    })
  }
})