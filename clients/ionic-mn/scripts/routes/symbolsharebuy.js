import {Route} from '../marionette.routing'
import ShareBuyView from '../views/newbuy'

export default Route.extend({

  viewClass: ShareBuyView,

  viewOptions() {
    return {
      model: this.buy,
      symbol: this.symbol
    }
  },

  activate(transition){
    let buy
    let symbol = this.getContext(transition).request('symbol');
    buy = symbol.buys.findWhere({id: transition.params.sharebuyid});
    if (!buy) {
      throw 'buy not found ' + transition.params.sharebuyid
    }
    this.buy = buy
    this.symbol = symbol
  }
})

