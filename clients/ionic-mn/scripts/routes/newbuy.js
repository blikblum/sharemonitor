import {Route} from '../marionette.routing'
import ShareBuyView from '../views/newbuy'

export default Route.extend({

  viewClass: ShareBuyView,

  viewOptions() {
    return {
      symbol: this.symbol
    }
  },

  activate(transition){
    this.symbol = this.getContext(transition).request('symbol');
  }
})
