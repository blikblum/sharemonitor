import Radio from 'backbone.radio'
import {Route} from '../marionette.routing'

export default Route.extend({

  contextRequests: {
    'symbol': 'getSymbol'
  },

  getSymbol() {
    return this.symbol
  },

  activate(transition){
    let ownedsymbols = Radio.channel('data').request('ownedsymbols')
    this.symbol = ownedsymbols.findWhere({id: transition.params.symbolid})
    this.symbol.loadBuys()
  }
})