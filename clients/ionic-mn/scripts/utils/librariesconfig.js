import _ from 'underscore'
import Backbone from 'backbone';
import Marionette from 'marionette';
import 'backbone.localstorage';
import 'rivets-backbone-adapter';
window.Behaviors = {};
window._ = _;
window.Backbone = Backbone;
window.Marionette = Marionette;
window.rivets = require('rivets');

var Validation = require('backbone-validation');

Validation.configure({
  forceUpdate: true
});

_.extend(Validation.messages, {
  required: 'Campo obrigatório'
});

//noinspection JSUnusedGlobalSymbols,JSUnusedGlobalSymbols
_.extend(Validation.callbacks, {
  valid: function (view, attr, selector) {
    var $label = view.$('[name=' + attr + ']').parent('label');

    $label.removeClass('has-error');
    $label.next('.form-error').remove();
  },
  invalid: function (view, attr, error, selector) {
    var $error;
    var $label = view.$('[name=' + attr + ']').parent('label');

    $label.addClass('has-error');
    $error = $label.next('.form-error');
    if ($error.length === 0) {
      $label.after('<div class="form-error">'+error+'</div>');
    } else {
      $error.text(error);
    }
  }
});


Marionette.Behaviors.behaviorsLookup = function() {
  return window.Behaviors;
}

rivets.configure(
  {
    templateDelimiters: ['{{', '}}']
  }
);

rivets.binders['class'] = function (el, value) {
  var classes = value ? value.trim().split(/\s+/) : [];
  console.log('class binder - classes ', classes, ' value: ', value, ' keypath: ', this.keypath);
  if(this.classesAdded) {
    this.classesAdded.forEach(function (className) {
      if (className) {
        el.classList.remove(className);
      }
    })
    this.classesAdded.length = 0;
  }

  if(classes.length) {
    classes.forEach(function (className) {
      if (className) {
        el.classList.add(className);
      }
    })
    this.classesAdded = classes
  }
};


rivets.formatters.default = function(value, defValue) {
  console.log('value', value, 'def', defValue);
  return value ? value : defValue;
};

rivets.formatters.lt = function(value, arg) {
  return value < arg;
};

rivets.formatters.eq = function (value, args) {
  return value === args;
};

rivets.formatters.negate = function (value) {
  return !value;
};

rivets.formatters.ifThen = function (value, truthyValue, falsyValue) {
  console.log('ifThen', value, truthyValue, falsyValue, arguments.length)
  if (value) {
    return truthyValue
  } else {
    if (arguments.length >= 3){
      return falsyValue
    }
  }
};

rivets.formatters.concatClass = function (value, addend) {
  value = value != null ? value : '';
  addend = addend != null ? addend : '';
  return value + ' ' + addend
};


rivets.formatters.ifConcatClass = function (value, addend) {
  if (value) {
    return value + ' ' + addend
  }
};

rivets.formatters.number = {
  read: function(value) {
    if (value != null) {
      return value.toString();
    }
  },
  publish: function(value) {
    if (value != null) {
      return +value;
    }
  }
};

rivets.formatters.formatFloat = function (value, decimals) {
  var decimals;
  if (arguments.length >= 2) {
    decimals = +arguments[1];
  }
  decimals = (decimals || decimals === 0) || 2;
  if (value == null) {
    return ''
  }  else {
    return  value.toFixed(decimals);
  }
};

rivets.formatters.formatCurrency = function(num) {
  if (num == null) {
    return '--'
  }  else {
    return 'R$' + num.toFixed(2);
  }
};


rivets.formatters.absolute = function (value) {
  if (value != null) {
    return Math.abs(value);
  }
};

rivets.formatters.percentage = function (value) {
  if (typeof value === 'undefined' || value === null) {
    return '--'
  }  else {
    return  (value * 100).toFixed(2) + '%';
  }
};

rivets.formatters.suffix = function (value, suffix) {
  if (value || value === 0) {
    return value + suffix
  }  else {
    return  value;
  }
};

rivets.formatters.toLocale = function (date) {
  if (typeof date === 'undefined' || date === null) {
    return '--'
  }  else {
    if (date instanceof Date) {
      return date.toLocaleDateString();
    } else if (typeof date === 'string') {
      return new Date(date).toLocaleDateString()
    }
  }
}

rivets.formatters.preventDefault = function(value) {
  return function(e, scope) {
    e.preventDefault();
    value.call(this, e, scope);
    return false;
  };
};
