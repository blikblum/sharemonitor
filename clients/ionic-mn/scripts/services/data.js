import {SymbolPriceStore, OwnedSymbol} from 'models';
import $ from 'jquery';

var symbols;
var priceStore;
var IPCAHistory;

var updateSymbolPrices = function (model) {
  console.log('Updating prices', model.changed);
  _.each(model.changed, function (value, key, list) {
    var symbol = symbols.findWhere({name: key});
    if (symbol) {
      symbol.set({latestPrice: value.price, latestVariation: value.variation});
      symbol.save();
    }
  });
};

var addSymbol = function (model) {
  priceStore.addSymbol(model.get('name'));
};

export default _.extend({}, {
  getOwnedSymbols: function(){
    var self = this;
    var deferred = $.Deferred();
    if (symbols) {
      deferred.resolveWith(self, [symbols]);
    } else {
      symbols = new OwnedSymbol.Collection({});
      //for debugging
      window.ownSymbols = symbols;
      symbols.fetch({
        success: function (collection) {
          if (!priceStore) {
            var priceAttrs = symbols.reduce(function (memo, model) {
              memo[model.get('name')] = null;
              return memo;
            }, {});
            console.log('priceAttrs', priceAttrs);
            priceStore = new SymbolPriceStore(priceAttrs);
            priceStore.on('change', updateSymbolPrices);
            priceStore.fetch();
            priceStore.startPolling();
            console.log('priceStore initialized');
            symbols.on('add', addSymbol);
          }
          deferred.resolveWith(self, [symbols]);
        },
        error: function () {
          console.log('Error fetching symbols', symbols);
          deferred.rejectWith(self, ['Error fetching symbols']);
        }
      })
    }
    return deferred.promise();
  },
  getIPCAHistory: function () {
    var deferred = $.Deferred();
    if (IPCAHistory) {
      deferred.resolveWith(null, [IPCAHistory])
    } else {
      $.getJSON('ipcahistory.json', null, function (data) {
        IPCAHistory = data;
        deferred.resolveWith(null, [IPCAHistory]);
      }).fail(function () {
        deferred.rejectWith(null, ['Error fetching IPCA history'])
      })
    }
    return deferred.promise()
  }
});
