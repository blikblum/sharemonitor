import rivets from 'rivets';

export default window.Behaviors.DataBinding = Marionette.Behavior.extend({
  initialize: function () {
    console.log('databinding initialize')
    this.view.template = false;
  },
  onBeforeRender: function() {
    console.log('databinding OnBeforeRender')
    var view = this.view;
    if (this._rview) {
      this._rview.unbind();
      this._rview = null;
    }
    this.view.attachElContent(view.html);
    this._rview = rivets.bind(this.el, view, {
      handler: function handler(context, ev, binding) {
        this.call(view, ev, binding.view.models);
      }
    });
  },
  onDestroy: function() {
    if (this._rview) {
      this._rview.unbind();
      this._rview = null;
    }
  }
}
);