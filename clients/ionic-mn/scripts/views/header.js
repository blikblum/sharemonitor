import Radio from 'backbone.radio';
import DataBinding from './behaviors/databinding';

export default Marionette.View.extend({
  el: '#app-header',
  behaviors: {DataBinding: DataBinding},
	html: require('../templates/header.html'),
	events: {
	  'click .title ~ .buttons button': 'onButtonClick',
    'click button.back-button': 'onBackButtonClick'
	},
	pageView: null,
	setPageView: function(view) {
    var viewOptions = _.result(view, 'pageHeader');
    viewOptions = _.mapObject(viewOptions, function (val, key, obj) {
      return _.isFunction(val) ? val.call(view) : val;
    });
    this.options = _.extend({title: '', backButton: false, buttons: [], className: ''}, viewOptions);
    this.pageView = view;
	},
    onButtonClick: function (e) {
        e.preventDefault();
        var method = e.currentTarget.dataset.method;
        console.log('method', method)
        if (method) {
            this.pageView.triggerMethod(method)
        }
    },
    onBackButtonClick: function (e) {
      e.preventDefault();
      Radio.channel('router').request('goBack');
    }
});