import Radio from 'backbone.radio';
import ionicPopup from '../ionic-jr/popup';
import DataBinding from './behaviors/databinding';
import dataservice from '../services/data'

var inflationData;

export default Marionette.View.extend({
  behaviors: {DataBinding: DataBinding},
  html: require('../templates/symboldetails.html'),
  initialize: function () {
    console.log('view Initialize')
    this.state = {};
  },
  events: {
    'click .js-add-buy': 'onAddBuyClick',
    'click #remove-symbol': 'onRemoveSymbolClick',
    'change #inflation-correction': 'onInflationCorrectionChange',
    'change #ir-correction': 'onIRCorrectionChange'
  },
  pageHeader: {
    title: function() {
      return this.model.get('name');
    },
    backButton: true
  },
  onBeforeRender: function () {
    console.log('view OnBeforeRender')
    this.updateValues()
  },
  onBuyClick: function onAddBuy(e, scope) {
    e.preventDefault();
    Radio.channel('router').request('transitionTo', 'symbolsharebuy', {symbolid: this.model.get('id'), sharebuyid: scope.buy.get('id')});
  },
  onAddBuyClick: function onAddBuy(e) {
    e.preventDefault();
    Radio.channel('router').request('transitionTo', 'symbolnewbuy');
  },
  onInflationCorrectionChange: function (e) {
    e.preventDefault();
    this.state.inflationCorrection = e.currentTarget.checked;
    dataservice.getIPCAHistory().then(data => {
      inflationData = data;
      this.updateValues();
    });
  },
  onIRCorrectionChange: function (e) {
    e.preventDefault();
    this.state.IRCorrection = e.currentTarget.checked;
    this.updateValues();
  },
  onRemoveSymbolClick: function onRemoveSymbol(e) {
    e.preventDefault();
    var confirmPopup = ionicPopup.confirm({
      title: 'Remover Símbolo',
      template: 'Confirme o que deseja fazer',
      okText: 'Excluir',
      cancelText: 'Manter'
    });
    var that = this;
    confirmPopup.then(function (res) {
      if (res) {
        that.model.destroy();
        Radio.channel('router').request('goBack');
      }
    });
  },
  updateValues: function () {
    var currentValue = this.model.getValue();
    var quantity = this.model.getStockQuantity();
    var meanPrice = this.state.inflationCorrection ? this.model.getInflatedMeanPrice(inflationData) : this.model.meanPrice;
    var buyValue = meanPrice * quantity;
    var valueDifference = currentValue - buyValue;
    if (currentValue > 20000 && valueDifference > 0 && this.state.IRCorrection) {
      valueDifference = valueDifference - (valueDifference * 0.15)
    }
    _.extend(this.state, {
      stockQuantity: quantity,
      stockValue: currentValue,
      valueVariation: valueDifference / buyValue,
      valueDifference: valueDifference,
      meanPrice: meanPrice,
      buyValue: buyValue
    })
  }
});