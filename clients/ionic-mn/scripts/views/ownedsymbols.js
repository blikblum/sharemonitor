import Radio from 'backbone.radio';
import DataBinding from './behaviors/databinding';

export default Marionette.View.extend({
  behaviors: {DataBinding: DataBinding},
  html: require('../templates/ownedsymbols.html'),
  pageHeader: {
    title: 'Carteira de Ações',
    buttons: [
        {
            icon: 'ion-plus',
            method: 'add:buy'
        }
    ]
  },
  onAddBuy: function () {
      Radio.channel('router').request('transitionTo', 'newbuy');
  },
  onItemClick: function (e, scope) {
    Radio.channel('router').request('transitionTo', 'symboldetails', {symbolid: scope.model.get('id')});
  }
});