import {ShareBuy, OwnedSymbol} from 'models';
import dataService from '../services/data';
import Radio from 'backbone.radio';
import $ from 'jquery';
import Validation from 'backbone-validation';
import ionicPopup from '../ionic-jr/popup';
import DataBinding from './behaviors/databinding';

export default Marionette.View.extend({
  html: require('../templates/sharebuy.html'),
  pageHeader: {
    title: function() {
      return this.pristineModel ? 'Editar Compra' : 'Nova Compra';
    },
    backButton: true,
    buttons: [
        {
            title: 'Salvar',
            method: 'save:buy',
            className: 'button-positive'
        }
    ]
  },
  behaviors: {
    DataBinding: DataBinding
  },
  initialize: function (options) {
    this.symbol = options.symbol;    
    if (this.model) {
      this.pristineModel = this.model;
      this.model = this.model.clone();
    } else {
      this.model = new ShareBuy.Model();
    } 
    this.state = {isNew: !this.pristineModel, hasSymbol: !!this.symbol};
    if (this.state.isNew && this.symbol) {
      this.model.set('symbol', this.symbol.get('name'));
    }
    this.listenTo(this.model, 'change:symbol', this.onSymbolChange);
    this.listenTo(this.model, 'change:price change:quantity', this.updateMeanPrice);
    Validation.bind(this);
  },
  events: {
    'blur input': 'onInputBlur',
    'click #remove-buy': 'onRemoveBuyClick'
  },
  getSymbol: function() {
    var d = $.Deferred()
    var symbolName = this.model.get('symbol');
    if (this.symbol) {
      d.resolveWith(null, [this.symbol])
    } else {
      if (symbolName) {
        dataService.getOwnedSymbols().then(function (ownedSymbols) {
          var symbol = ownedSymbols.findWhere({name: symbolName});
          if (!symbol) {
            symbol = new OwnedSymbol.Model({name: symbolName});
          }       
          d.resolveWith(null, [symbol])
        });  
      } else {
        //todo
      }              
    }
    return d.promise();
  },

  onRender: function () {
    this.updateMeanPrice();
  },
  onDestroy: function() {
    Validation.unbind(this);
  },
  onSymbolChange: function () {
    var self = this;
    if (this.symbol) {
      this.stopListening(this.symbol)
    }
    this.symbol = null;
    this.getSymbol().then(function (symbol) {
      self.symbol = symbol;
      self.updateMeanPrice();
    });
  },
  updateMeanPrice: function() {
    this.state.meanPrice = this.symbol ? this.symbol.simulateMeanPrice(this.model) : undefined;
    this.state.currentMeanPrice = this.symbol ? this.symbol.meanPrice : undefined;
  },
  onMeanPriceChange: function () {
    this.updateMeanPrice();
  },
  onInputBlur: function(e) {
    var inputName = e.currentTarget.name;
    //symbol is updated on blur
    _.defer(function (model, name) {
      model.isValid([name]);
    }, this.model, inputName);

  },
  onRemoveBuyClick: function (e) {
    e.preventDefault();
    var confirmPopup = ionicPopup.confirm({
      title: 'Remover Compra',
      template: 'Confirme o que deseja fazer',
      okText: 'Excluir',
      cancelText: 'Manter'
    });
    var that = this;
    confirmPopup.then(function (res) {
      if (res) {
        that.pristineModel.destroy();
        Radio.channel('router').request('goBack');
      } 
  });    
  },
  onSaveBuy: function () {
    var self = this;
    if (!this.model.isValid(true)) return;
    if (this.pristineModel) {
      this.pristineModel.save(this.model.attributes);
      Radio.channel('router').request('goBack');
    } else {
      $.when(dataService.getOwnedSymbols(), this.getSymbol())
        .done(function (ownedSymbols, symbol) {
          if (ownedSymbols.indexOf(symbol) === -1) {
            ownedSymbols.add(symbol);
            symbol.save();
          }
          console.log('addBuy', self.model.toJSON());
          //todo add model directly
          symbol.addBuy(self.model.toJSON());
          Radio.channel('router').request('goBack');
        });
    }
      //todo: add error handling
  }
});
