var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: "./scripts/main.js",
  output: {
    path: __dirname + '/build',
    filename: "bundle.[hash].js"
  },
  devtool: "source-map",
  resolve: {
    root: [path.resolve('.')],
    alias: {
      marionette: 'backbone.marionette'
    },
    fallback: __dirname + '/node_modules'
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: __dirname + '/index.html',
      template: 'index-template.html'
    }),
    new CleanWebpackPlugin(['build'], {
      root: __dirname,
      verbose: true,
      dry: false,
      exclude: ['ionic.js', '.gitignore']
    })
  ],
  module: {
    loaders: [
      {test: /\.html$/, loader: 'html-loader'},
      {
        test: /\.js$/, loader: 'babel?presets[]=es2015', include: [
        path.resolve(__dirname, "scripts")
      ]
      }
    ]
  }
};