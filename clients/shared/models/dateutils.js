var epoch = new Date(1899, 11, 30);
var msPerDay = 8.64e7;

function toOADate(d) {
  var v = -1 * (epoch - d) / msPerDay;

  // Deal with dates prior to 1899-12-30 00:00:00
  var dec = v - Math.floor(v);

  if (v < 0 && dec) {
    v = Math.floor(v) - dec;
  }

  return v;
};

function fromOADate(n) {
  // Deal with -ve values
  var dec = n - Math.floor(n);

  if (n < 0 && dec) {
    n = Math.floor(n) - dec;
  }

  return new Date(n * msPerDay + +epoch);
}

module.exports = {
  fromOADate: fromOADate,
  toOADate: toOADate
}