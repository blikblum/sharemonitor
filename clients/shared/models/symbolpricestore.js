var fetchPrice = function (model) {
  console.log('fetching price store');
  model.fetch();
};

var SymbolPriceStore = Backbone.Model.extend({
  getURL: function (symbolNames) {
    symbolNames = symbolNames || this.keys().join(',');
    return '../cgi-bin/sharemonitor.cgi/LatestStockPrice?Symbols=' + symbolNames;
  },
  url: function () {
    return this.getURL();
  },
  intervalID: 0,
  addSymbol: function(symbolName) {
    this.fetch({url: this.getURL(symbolName)})
  },
  startPolling: function(interval) {
    if (this.intervalID) {
      clearInterval(this.intervalID)
    }
    this.intervalID = setInterval(fetchPrice, 1000 * 60 * 15, this);
  },
  stopPolling: function() {
    if (this.intervalID) {
      clearInterval(this.intervalID);
      this.intervalID = 0;
    }
  }
});

module.exports = SymbolPriceStore;