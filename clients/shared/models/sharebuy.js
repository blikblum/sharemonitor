//var Backbone = require('backbone');
//var _ = require('underscore');

var Computed = require('backbone-computedfields');

var ShareBuy = Backbone.Model.extend({
  defaults: function () {
    return {
      date: (new Date()).toISOString().slice(0,10)
    }
  },
  initialize: function () {
    this.computedFields = new Computed(this);
  },
  computed: {
    totalPrice: {
      depends: ['price', 'quantity'],
      get: function () {
        return this.get('price') * this.get('quantity');
      },
      toJSON: false
    }
  },
  validation: {
    symbol: {
      required: true
    },
    price: {
      required: true,
      min: 0.1,
      msg: 'Preço deve ser maior que 0'
    },
    quantity: {
      required: true,
      min: 0.1,
      msg: 'Quantidade deve ser maior que 0'
    },
    date: {
      required: true,
      fn: function(value, attr, computedState) {
        if (isNaN(Date.parse(value))) {
          return 'Formato de data inválido'
        }
      }
    }
  },
  symbol: ''
});

var ShareBuys = Backbone.Collection.extend({
  model: ShareBuy,
  localStorage: new Backbone.LocalStorage('sharebuy'),
  parse: function(response) {
    return _.where(response, {symbol: this.symbol})
  }
});

module.exports = {Model: ShareBuy, Collection: ShareBuys};
