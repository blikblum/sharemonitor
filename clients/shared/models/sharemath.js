var toOADate = require('./dateutils').toOADate

function meanPrice(buys) {
  var division = _.reduce(buys, function (memo, buy) {
    memo[0] += buy.price * buy.quantity;
    memo[1] += buy.quantity;
    return memo;
  }, [0, 0]);
  return division[1] ? division[0]/division[1] : 0;
};

function getInflationFromDate(date, inflationData) {
  var months, rates, result;
  months = _.filter(inflationData, function (month) {
    return month.date > date
  });

  if (!months.length) {
    return 0
  }

  rates = _.map(months, function (month) {
   return 1 + (month.value / 100)
  });

  result = _.reduce(rates, function (memo, value) {
    return memo * value
  }) || 0;

  if (result) {
    result -= 1
  }

  return result;
}

function applyInflation(buy, inflationData) {
  console.log('buy', buy);
  var buyDate = toOADate(new Date(buy.date));
  var rate = getInflationFromDate(buyDate, inflationData);
  console.log('rate', rate);
  buy.price = buy.price * (1 + rate)
}

module.exports = {
  meanPrice: meanPrice,
  applyInflation: applyInflation
};
