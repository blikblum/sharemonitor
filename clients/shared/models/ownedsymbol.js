var ShareBuyEntities = require('./sharebuy');
var sharemath = require('./sharemath');

var OwnedSymbol = Backbone.Model.extend({
  defaults: {
    name: '',
    latestPrice: null,
    latestVariation: null
  },

  meanPrice: 0,

  initialize: function () {
    this.on('destroy', this.onDestroy, this);
    this.buys = new ShareBuyEntities.Collection();
    this.buys.on('update reset destroy change:price change:quantity', this._updateMeanPrice, this);
  },

  onDestroy: function () {
    var model;

    while (model = this.buys.first()) {
      model.destroy();
    }
  },

  loadBuys: function () {
    //todo: make it a promise - getBuys
    if (this.buys.symbol !== this.get('name')){
      this.buys.symbol = this.get('name');
      this.buys.fetch({
        success: function () {
        }.bind(this)
      });
    }
  },

  addBuy: function(attrs) {
    this.loadBuys();
    this.buys.create(_.extend(attrs, {symbol: this.get('name')}));
  },

  getStockQuantity: function () {
    return this.buys.reduce(function (memo, model) {
      return memo += model.get('quantity')
    }, 0)
  },

  getValue: function(stockPrice){
    stockPrice = stockPrice || this.get('latestPrice');
    return this.buys.reduce(function(memo, model) {
      memo += model.get('quantity') * stockPrice;
      return memo;
    }, 0);
  },

  getInflatedMeanPrice: function (inflationData) {
    var inflationFn = _.partial(sharemath.applyInflation, _, inflationData);
    var buysData = this.buys.toJSON();
    _.each(buysData, inflationFn);
    return sharemath.meanPrice(buysData);
  },

  simulateMeanPrice: function (buy) {
    var buysData = this.buys.toJSON();
    var loadedBuy = _.findWhere(buysData, {id: buy.get('id')});
    if (loadedBuy) {
      loadedBuy.price = buy.get('price');
      loadedBuy.quantity = buy.get('quantity');
    } else {
      buysData.push(buy.toJSON());
    }
    return sharemath.meanPrice(buysData);
  },

  _updateMeanPrice: function () {
    var buysData = this.buys.toJSON();
    this.meanPrice = sharemath.meanPrice(buysData);
    this.trigger('change:meanPrice');
    console.log('meanPrice', this.meanPrice);
  }
});

var OwnedSymbols = Backbone.Collection.extend({
  localStorage: new Backbone.LocalStorage('ownedsymbol'),
  model: OwnedSymbol,
  initialize: function () {
  }
});

module.exports = {
  Model: OwnedSymbol,
  Collection: OwnedSymbols
};
