# README #

This is a mobile web app to monitor share prices in [Bovespa](http://www.bmfbovespa.com.br/pt_br/).

Is used as a test bed for my MarionetteJS learning and for personal usage. There's no pretension to be a finished, user ready app.

Uses Ionic for CSS and some utilities

### How do I get set up? ###

* npm install
* webpack