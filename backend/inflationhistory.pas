unit InflationHistory;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpjson, fpspreadsheet;

type

  { TIPCAHistory }

  TIPCAHistory = class
  private
    FData: TJSONArray;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadFromFile(const FileName: String);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveFromFile(const FileName: String);
    property Data: TJSONArray read FData;
  end;

implementation

uses
  fpsallformats, fpsTypes, strutils;

{ TIPCAHistory }

constructor TIPCAHistory.Create;
begin
  FData := TJSONArray.Create;
end;

destructor TIPCAHistory.Destroy;
begin
  FData.Destroy;
  inherited Destroy;
end;

procedure TIPCAHistory.LoadFromFile(const FileName: String);
var
  WB: TsWorkbook;
  WS: TsWorksheet;
  MonthData: TJSONObject;
  MonthDate: Int64;
  MonthStr: String;
  Month, Year: Integer;
  Row: Cardinal;
  IPCA: Double;
begin
  FData.Clear;

  WB := TsWorkbook.Create;
  try
    WB.ReadFromFile(FileName);
    WS := WB.GetFirstWorksheet;
    Row := 1;
    while Row <= WS.GetLastRowIndex do
    begin
      Year := Trunc(WS.ReadAsNumber(Row, 0));
      if Year >= 1994 then
      begin
        MonthStr := Trim(WS.ReadAsText(Row, 1));
        while MonthStr <> '' do
        begin
          Month := AnsiIndexText(MonthStr, ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL',
            'AGO', 'SET', 'OUT', 'NOV', 'DEZ']) + 1;
          MonthDate := Trunc(EncodeDate(Year, Month, 1));
          IPCA := WS.ReadAsNumber(Row, 3);
          MonthData := TJSONObject.Create(['date', MonthDate, 'value', IPCA]);
          FData.Add(MonthData);
          Inc(Row);
          MonthStr := Trim(WS.ReadAsText(Row, 1));
        end;
      end
      else
        Inc(Row);
    end;
  finally
    WB.Destroy;
  end;
end;

procedure TIPCAHistory.LoadFromStream(Stream: TStream);
var
  WB: TsWorkbook;
  WS: TsWorksheet;
  MonthData: TJSONObject;
  MonthDate: Int64;
  MonthStr: String;
  Month, Year: Integer;
  Row: Cardinal;
  IPCA: Double;
begin
  FData.Clear;

  WB := TsWorkbook.Create;
  try
    WB.ReadFromStream(Stream, sfExcel8);
    WS := WB.GetFirstWorksheet;
    Row := 1;
    while Row <= WS.GetLastRowIndex do
    begin
      Year := Trunc(WS.ReadAsNumber(Row, 0));
      if Year >= 1994 then
      begin
        MonthStr := Trim(WS.ReadAsText(Row, 1));
        while MonthStr <> '' do
        begin
          Month := AnsiIndexText(MonthStr, ['JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL',
            'AGO', 'SET', 'OUT', 'NOV', 'DEZ']) + 1;
          MonthDate := Trunc(EncodeDate(Year, Month, 1));
          IPCA := WS.ReadAsNumber(Row, 3);
          MonthData := TJSONObject.Create(['date', MonthDate, 'value', IPCA]);
          FData.Add(MonthData);
          Inc(Row);
          MonthStr := Trim(WS.ReadAsText(Row, 1));
        end;
      end
      else
        Inc(Row);
    end;
  finally
    WB.Destroy;
  end;
end;

procedure TIPCAHistory.SaveFromFile(const FileName: String);
begin

end;

end.

