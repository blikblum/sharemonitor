unit MainModule;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Classes, httpdefs, fpHTTP, fpWeb;

type

  { TWebModule }

  TWebModule = class(TFPWebModule)
    procedure LatestStockPriceRequest(Sender: TObject; ARequest: TRequest;
      AResponse: TResponse; var Handled: Boolean);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  WebModule: TWebModule;

implementation

uses
  httpsend, fpjson, DOM, XMLRead, LuiJSONUtils;

{$R *.lfm}

function GetStockInfoBovespaXML(Stream: TStream): TJSONObject;
var
  ResponseDoc: TXMLDocument;
  SymbolNode, PriceNode, VariationNode: TDOMNode;
  Price, Variation: Double;
  Settings: TFormatSettings;
begin
  Result := TJSONObject.Create(['price', nil, 'variation', nil]);
  Stream.Position := 0;
  ReadXMLFile(ResponseDoc, Stream);
  Settings := DefaultFormatSettings;
  Settings.DecimalSeparator := ',';
  try
    SymbolNode := ResponseDoc.DocumentElement.FindNode('PAPEL');
    if SymbolNode <> nil then
    begin
      PriceNode := SymbolNode.Attributes.GetNamedItem('VALOR_ULTIMO');
      if PriceNode <> nil then
      begin
        if TryStrToFloat(PriceNode.NodeValue, Price, Settings) then
          Result.Floats['price'] := Price;
      end;
      VariationNode := SymbolNode.Attributes.GetNamedItem('OSCILACAO');
      if VariationNode <> nil then
      begin
        if TryStrToFloat(VariationNode.NodeValue, Variation, Settings) then
          Result.Floats['variation'] := Variation;
      end;
    end;
  finally
    ResponseDoc.Free;
  end;
end;

procedure FetchBovespaData(Client: THTTPSend; ResponseData: TJSONObject; SymbolList: TStrings);
const
  BovespaURL = 'http://www.bmfbovespa.com.br/cotacoes2000/formCotacoesMobile.asp?codsocemi=%s';
var
  SymbolName: String;
  i: Integer;
  StockData: TJSONObject;
begin
  for i := 0 to SymbolList.Count - 1 do
  begin
    SymbolName := SymbolList[i];
    ResponseData.Nulls[SymbolName] := True;
    if Client.HTTPMethod('GET', Format(BovespaURL, [SymbolName])) then
    begin
      //todo: cache
      StockData := GetStockInfoBovespaXML(Client.Document);
      ResponseData.Objects[SymbolName] := StockData;
      Client.Clear;
    end
    else
    begin
      //todo: log error
      //AResponse.Contents.Add('Error retrieving remote data: ' + Format(BovespaURL, [SymbolName]));
    end;
  end;
end;

procedure LoadStockInfoYahoo(Stream: TStream; StocksData: TJSONObject);
var
  ResponseData: TJSONObject;
  Count, i: Integer;
  SymbolName, PriceStr, VariationStr, ResourcePath: String;
  Variation, Price: Double;
  Settings: TFormatSettings;
begin
  Settings := DefaultFormatSettings;
  Settings.DecimalSeparator := '.';
  if TryStreamToJSON(Stream, ResponseData) then
  begin
    if FindJSONPath(ResponseData, 'list.meta.count', Count) then
    begin
      for i := 0 to Count - 1 do
      begin
        ResourcePath := Format('list.resources[%d].resource.fields', [i]);
        if not FindJSONPath(ResponseData, ResourcePath + '.symbol', SymbolName) then
          Continue;
        if not FindJSONPath(ResponseData, ResourcePath + '.price', PriceStr) then
          Continue;
        if not FindJSONPath(ResponseData, ResourcePath + '.chg_percent', VariationStr) then
          Continue;
        TryStrToFloat(VariationStr, Variation, Settings);
        TryStrToFloat(PriceStr, Price, Settings);
        SymbolName := Copy(SymbolName, 1, Length(SymbolName) - 3);
        StocksData.Objects[SymbolName] := TJSONObject.Create([
          'price', Price,
          'variation', Variation
        ]);
      end;
    end;
    ResponseData.Destroy;
  end;
end;

procedure FetchYahooData(Client: THTTPSend; ResponseData: TJSONObject; SymbolList: TStrings);
const
  YahooURL = 'http://finance.yahoo.com/webservice/v1/symbols/%s/quote?format=json&view=detail';
var
  SymbolName, SymbolQuery: String;
  i: Integer;
begin
  SymbolQuery := '';
  for i := 0 to SymbolList.Count - 1 do
  begin
    SymbolName := SymbolList[i];
    ResponseData.Nulls[SymbolName] := True;
    SymbolQuery := SymbolQuery + SymbolName + '.SA';
    if i < SymbolList.Count - 1 then
      SymbolQuery := SymbolQuery + ',';
  end;

  if Client.HTTPMethod('GET', Format(YahooURL, [SymbolQuery])) then
  begin
    LoadStockInfoYahoo(Client.Document, ResponseData);
    Client.Clear;
  end
  else
  begin
    //todo: log error
    //AResponse.Contents.Add('Error retrieving remote data: ' + Format(BovespaURL, [SymbolName]));
  end;
end;

{ TWebModule }

procedure TWebModule.LatestStockPriceRequest(Sender: TObject; ARequest: TRequest;
  AResponse: TResponse; var Handled: Boolean);
var
  Client: THTTPSend;
  SymbolList: TStringList;
  ResponseData: TJSONObject;
begin
  Handled := True;
  Client := THTTPSend.Create;
  SymbolList := TStringList.Create;
  ResponseData := TJSONObject.Create;
  try
    SymbolList.Delimiter := ',';
    SymbolList.StrictDelimiter := True;
    SymbolList.DelimitedText := ARequest.QueryFields.Values['Symbols'];
    FetchYahooData(Client, ResponseData, SymbolList);
    AResponse.Contents.Add(ResponseData.AsJSON);
  finally
    ResponseData.Destroy;
    SymbolList.Destroy;
    Client.Destroy;
  end;
end;

initialization
  RegisterHTTPModule('TWebModule', TWebModule);
end.

