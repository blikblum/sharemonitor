program UpdateInflationHistory;

{$mode objfpc}{$H+}

uses
  Classes, sysutils, InflationHistory, LuiJSONUtils, ftpsend, AbZipTyp, AbUnzPrc, AbArcTyp;

type

  { TZipCallbacks }

  TZipCallbacks = object
    procedure UnzipProc(Sender : TObject; Item : TAbArchiveItem;
                        const NewName : string );
    procedure UnzipToStreamProc(Sender : TObject; Item : TAbArchiveItem;
                                OutStream : TStream);
  end;

var
  ZipCallbacks: TZipCallbacks;

function GetSpreadsheetStream: TStream;
var
  Client: TFTPSend;
  ZipArchive: TAbZipArchive;
begin
  Result := nil;
  //download: //ftp://ftp.ibge.gov.br/Precos_Indices_de_Precos_ao_Consumidor/IPCA/Serie_Historica/ipca_201508SerieHist.zip
  Client := TFTPSend.Create;
  try
    //download
    Client.TargetPort := '21';
    Client.TargetHost := 'ftp.ibge.gov.br';
    if Client.Login then
    begin
      Client.ChangeWorkingDir('Precos_Indices_de_Precos_ao_Consumidor/IPCA/Serie_Historica');
      if Client.RetrieveFile('ipca_SerieHist.zip', False) then
      begin
        //unzip
        ZipArchive := TAbZipArchive.CreateFromStream(Client.DataStream, 'ipca_SerieHist.zip');
        try
          ZipArchive.ExtractHelper := @ZipCallbacks.UnzipProc;
          ZipArchive.ExtractToStreamHelper := @ZipCallbacks.UnzipToStreamProc;
          ZipArchive.Load;
          if ZipArchive.ItemList.Count > 0 then
          begin
            Result := TMemoryStream.Create;
            try
              ZipArchive.ExtractToStream(ZipArchive.Items[0].FileName, Result);
              Result.Position := 0;
            except
              FreeAndNil(Result);
            end;
          end;
        finally
          ZipArchive.Destroy;
        end;
      end;
    end;
  finally
    Client.Destroy;
  end;
end;

var
  SpreadsheetStream: TStream;
  History: TIPCAHistory;

{ TCallbackHelper }

procedure TZipCallbacks.UnzipProc(Sender: TObject; Item: TAbArchiveItem; const NewName: string);
begin
  AbUnzip( TAbZipArchive(Sender), TAbZipItem(Item), NewName);
end;

procedure TZipCallbacks.UnzipToStreamProc(Sender: TObject; Item: TAbArchiveItem;
  OutStream: TStream);
begin
  AbUnzipToStream(TAbZipArchive(Sender), TAbZipItem(Item), OutStream);
end;

begin
  SpreadsheetStream := GetSpreadsheetStream;
  if SpreadsheetStream <> nil then
  begin
    History := TIPCAHistory.Create;
    History.LoadFromStream(SpreadsheetStream);
    WriteJSONFile(History.Data, 'ipcahistory.json', []);
    SpreadsheetStream.Destroy;
    History.Destroy;
  end;
end.

